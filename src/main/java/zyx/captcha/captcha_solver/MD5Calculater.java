package zyx.captcha.captcha_solver;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author xcoder
 */
public class MD5Calculater {
    BufferedImage bi;
        byte[] barray;
        Path PATH_TO_FILE;
        
    public String md5(){
        PATH_TO_FILE = (Path) Paths.get(Utilities.PATH_TO_FILE);
        
        try {
            bi = ImageIO.read(new File(Utilities.PATH_TO_FILE));
        } catch (IOException ex) {
            Logger.getLogger(MD5Calculater.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(bi, "jpg", baos);
                baos.flush();
                barray = baos.toByteArray();
            } catch (IOException ex) {
            Logger.getLogger(MD5Calculater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return DigestUtils.md5Hex(Arrays.toString(barray));
    }
}
